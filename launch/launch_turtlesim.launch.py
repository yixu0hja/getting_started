from launch import LaunchDescription
from launch.actions import TimerAction
from launch.actions import ExecuteProcess
from launch.actions import LogInfo
import launch_ros.actions


def generate_launch_description():
    return LaunchDescription([
        TimerAction(period=10.0, actions=[
            LogInfo(msg='Stopping turtle control'),
            ExecuteProcess(
                cmd=['/opt/ros/galactic/bin/ros2', 'topic',
                     'pub', '--once', '/stop', 'std_msgs/Bool',
                     '{data: true}',
                     ],
                emulate_tty=True,
                output="both"
            )
        ]),
        launch_ros.actions.Node(
            name='turtlesim_node',
            package='turtlesim',
            executable='turtlesim_node',
            output='screen',
        ),
    ])
