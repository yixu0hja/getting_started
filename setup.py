from setuptools import setup

package_name = 'getting_started'

setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/launch',
         ['launch/launch_turtlesim.launch.py']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Angus Stewart',
    maintainer_email='siliconlad@protonmail.com',
    url='https://gitlab.com/eufs/admin/onboarding/getting_started',
    description='A template package for the EUFS Driverless Software onboarding process.',
    license='MIT',
    tests_require=['pytest'],
)
