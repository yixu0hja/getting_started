# Getting Started

A template package with an exercise to get started learning ROS.

## Create a Workspace

The first step is to clone the repository into your ROS workspace.

You can create a workspace, and call it `dev_ws`, by constructing the following file structure:

```text
dev_ws/
    src/
```

Then inside the `src` directory, [`git clone`](https://education.github.com/git-cheat-sheet-education.pdf) this repository using the following command:

```bash
git clone https://gitlab.com/eufs/admin/onboarding/getting_started.git
```

## Exercise

In this exercise, we will create a new node in a ROS package to control a turtle in the [turtlesim environment](https://ros2-industrial-workshop.readthedocs.io/en/latest/_source/basics/ROS2-Turtlesim.html).

The requirements for your node are as follows:

1. Must publish to the `/turtle1/cmd_vel` topic at 10Hz. How the turtle moves is up to you!
2. Must subscribe to the `/stop` topic. When you receive a truth value, you should terminate your node.
3. Periodically change the colour of the pen by utilizing the `/turtle1/set_pen` service.

### 0. Launch turtlesim

The repository already contains a package named "`getting_started`". In the package there already exists a launch file, `launch/launch_turtlesim.launch.py`.

To use this launch file, and your new node in the future, the package must be built. Building must always be done in a workspace. To build everything in the workspace, run the `colcon build` command from the workspace directory; `dev_ws` for this exercise:

```bash
cd dev_ws
colcon build
```

The launch file can then be run using the `launch` command from the ROS 2 CLI. For information about how to use it, run the command `ros2 launch --help`. Use this help to run the provided launch file (hint: no optional arguments or launch arguments need to be passed, just the package name and launch file name).

> **Make sure the ROS 2 installation workspace is sourced in the terminal** (using `source /opt/ros/galactic/setup.sh`). The error "command not found: ros2" means this was not done.

> **Also make sure to source the built workspace as an overlay** (using `. install/setup.sh` from the workspace directory). The error "Package 'getting_started' not found: ..." means this was not done.

The GUI for the turtlesim will be launched by the launch file. This will show a turtle sitting still in the middle of an ocean. The node you will create will make the turtle move around the ocean. [This workshop](https://ros2-industrial-workshop.readthedocs.io/en/latest/_source/basics/ROS2-Turtlesim.html) goes over the turtlesim in some more detail than needed for this getting started, but may be useful.

Additionally, after 10 seconds there should be a console output saying `"Stopping turtle control"`. This happens when a message is published to the `/stop` topic of type `std_msgs/msg/Bool` that has the attribute `data` with the boolean value of `true` (this is done by the `TimerAction` on line 11 in the launch file).

To view the definition of the `std_msgs/msg/Bool` message type, run the following command:

```bash
ros2 interface show std_msgs/msg/Bool
```

The `ros2 interface show` command can be used to view the definition of any specified message or service type.

### 1. Create Node with Publisher

We now need to create a ROS node inside the existing `getting_started` package and add a publisher to it.

To do this create a new python file inside the `getting_started` folder (which is a Python module with the same name as the ROS package), inside the `getting_started` package, alongside the `__init__.py` file. You can name this file whatever you like, but generally the convention is the node name in snake_case followed by the `.py` file extension.

For a tutorial showing how to create a node with a publisher, see the "**2.1 Examine the code**" section in [this ROS tutorial](https://docs.ros.org/en/galactic/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Publisher-And-Subscriber.html#examine-the-code).

**Do not follow the entire tutorial**, only the section outlined above. **Write the code yourself**, do not copy and paste the code from the tutorial. This will help you learn the concepts better. Additionally the code will not be the same.

Also a reminder that the requirement of the node is to publish to the `/turtle1/cmd_vel` topic, at 10Hz. To figure out the message type of the topic, you can use the `ros2 topic info` command. For example, to get information about the `/turtle1/cmd_vel` topic, launch the turtlesim using the launch file, then run the following command:

```bash
ros2 topic info /turtle1/cmd_vel
```

To use the message type in your code, the linked tutorial (see above) can be used as a reference. There, the publisher's message type is `std_msgs/msg/String`. This is imported using:

```python
from std_msgs.msg import String
```

[`std_msgs`](https://docs.ros2.org/galactic/api/std_msgs/index-msg.html) is one of many available ROS packages. `std_msgs.msg` is a Python module inside the package containing many different message classes, including `String`. Once imported, the `String` class is passed into the `create_publisher` function. This basic idea can be modified to import and use the necessary message type that you found with `ros2 topic info`.

### 2. Launching the Node

Now you have created the node, you can set it up to be launched with the turtlesim.

Firstly, an entry point needs to be added for the node to be able to run from the command line. This is done by adding the following to the `setup.py` file in the `getting_started` package:

```python
entry_points={
    'console_scripts': [
        '<name of node> = getting_started.<file name without extension>:main',
    ],
},
```

replacing `<name of node>` with the name of the node you created, and `<file name without extension>` with the name of the file you created (without the `.py` extension).

It can now be added to the **existing** `launch_turtlesim.launch.py` launch file to be launched together with the turtlesim.

For a tutorial showing how to write a launch file, see the "**3 Writing the launch file**" section [here](https://docs.ros.org/en/galactic/Tutorials/Intermediate/Launch/Launch-system.html#writing-the-launch-file).

**Again, do not follow the entire tutorial and do not copy and paste from the code in the tutorial.**

Now (after rebuilding) when running the launch file, rather than the turtle sitting still in the middle of the ocean, it should move around based on the messages published by your node. Update the code in your node to make the turtle move how you want.

### 3. Create Subscriber

Once you have a node with a publisher, you should add a subscriber **to the same node**, as outlined in the requirements.

For a tutorial showing how to create a node with a subscriber, see the "**3.1 Examine the code**" section [here](https://docs.ros.org/en/galactic/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Publisher-And-Subscriber.html#id1).

**As you should add a subscriber to the node you already created, only a few lines of the code in the tutorial need to be included.**

Reminder to use the `ros2 topic info` command to get information about the message type from the topic to subscribe to. Then import the message type to use it when creating the subscription.

Make sure that now (after rebuilding) when running the launch file, the turtle will stop the node after 10 seconds, due to the node being terminated.

### 4. Create Service Client

Finally, you should create the service client **in the same node**, as outlined in the requirements.

For a tutorial showing how to create a node with a service client, see the "**3 Write the client node**" section [here](https://docs.ros.org/en/galactic/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Service-And-Client.html#write-the-client-node).

**Again, only a few lines from the tutorial need to be included.**

For a service, the `ros2 service type` command can be used to get its type. For example, to get the type of the `/turtle1/set_pen` service, launch the turtlesim using the launch file, then run the following command:

```bash
ros2 service type /turtle1/set_pen
```

The definition of service type can be found in the same way as message types, using the `ros2 interface show` command. Importing the service type is also done in a similar way to message types, using `from <package_name>.srv import <ServiceName>`.

Now (after rebuilding) when running the launch file, the color of the line drawn by the turtle should be as you specified in the request to the service.

### 5. Visualization

You can also use GUIs to help you visualize data and interact with services. If you're interested, a good first step is to look at [this tutorial](https://docs.ros.org/en/galactic/Tutorials/Beginner-CLI-Tools/Introducing-Turtlesim/Introducing-Turtlesim.html#install-rqt).

RQT is also a useful tool to visualise data from topics easily. Give it a go by running the `rqt` command in a terminal, then selecting a plugin from `Plugins` in the menu bar!

The following are some suggested plugins to have a look at:

- `Introspection > Node Graph`: This shows the nodes and topics in the system, and how they are connected.
- `Topics > Topic Monitor`: This shows the information about all topics.
- `Topics > Message Publisher`: This allows you to publish messages to a topic.
- `Topics > Message Type Browser`: This allows you to view the definition of a message type.
- `Services > Service Caller`: This allows you to call a service.
- `Services > Service Type Browser`: This allows you to view the definition of a service type.
- `Visualization > Plot`: Plots data from a topic over time.

### 6. Extensions

If you're interested in exploring some other features of ROS for your node, here are some ideas:

- Add ROS parameters to your node to make the motion of the turtle configurable. These can then be set in the launch file. See [this tutorial](https://docs.ros.org/en/galactic/Tutorials/Beginner-Client-Libraries/Using-Parameters-In-A-Class-Python.html#build-and-run) for an example of how to do this.
- Make the turtle controllable through the keyboard using the `teleop_twist_keyboard` node from the [`teleop_twist_keyboard` package](https://github.com/ros2/teleop_twist_keyboard).
- (Advanced) Convert your node into a "lifecycle node". For more information, read [this README from ROS demos](https://github.com/ros2/demos/blob/foxy/lifecycle/README.rst).

## Conclusion

Congratulations on reaching the end of this document. Hopefully, you feel a little more comfortable with ROS!

If you have any thoughts on how to improve this guide, please let us know by [creating an issue](https://gitlab.com/eufs/admin/onboarding/getting_started/-/issues) in this repository!
